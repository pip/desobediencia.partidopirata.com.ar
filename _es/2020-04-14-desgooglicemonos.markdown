---
title: Usando Jitsi Meet sin renunciar a la privacidad
description: 'Jitsi Meet es la plataforma de videoconferencias libre pero sólo funciona en Google Chrome...'
author:
- Partido Interdimensional Pirata
categories:
- Internet
layout: post
---

Venimos usando y recomendando Jitsi Meet hace bastante tiempo para
videoconferencias libres.  Hasta unx pirata tiene un [proyecto para
medir la velocidad de los distintos
servidores](https://ladatano.partidopirata.com.ar/jitsimeter/)!  Jitsi
Meet nos permite encontrarnos, vernos, hablarnos sin entregar datos
a terceros como hace Zoom, por ejemplo.  Funciona muy bien (tan bien
como puede funcionar la videoconferencia en general) y no hay que
instalar nada en la compu, si ya tenés algún navegador basado en
Chromium (ver más abajo :).  También nos gusta
[Mumble](https://es.wikipedia.org/wiki/Mumble) P)

Sin embargo, nos encontramos con que para que funcione correctamente hay
que utilizar Chrome, el navegador de Google.  En Firefox y otros
navegadores libres y un poco más aliados con nuestros objetivos, todavía
hay algunos problemas que se están resolviendo en este momento, desde
ambas partes.

Mientras tanto nos encontramos con la contradicción de tener que
recomendar un programa espía para poder acceder a uno que no lo es.
Siempre preferimos comunicarnos, así que la elección pragmática era
fácil pero no nos dejaba de dar escalofríos.

![Por qué no tenemos las dos cosas?](https://i.giphy.com/media/3o85xIO33l7RlmLR4I/giphy.webp)

Encontramos un proyecto llamado _Ungoogled Chromium_ (Chromium
Desgooglizado) que logra específicamente esto.  Ahora podemos usar Jitsi
Meet sin que Google esté escuchando lo que hacemos e incluso reemplazar
Chrome si lo estábamos usando P)

Para instalarlo, hay que [visitar su página de versiones
publicadas](https://ungoogled-software.github.io/ungoogled-chromium-binaries/)
y elegir la descarga para nuestro sistema operativo.  Las adecuadas son:

* GNU/Linux: La correspondiente para tu distribución, o si estás en duda
  la versión Portable de 64 bits.

* Windows: la versión de Windows para 64 bits.

* OSX: la única que hay, dice macOS.

Si alguna de las versiones de 64 bits no te funciona, podés probar con
la de 32 bits.

## Desgooglicémonos!

Si estabas usando Chromium antes, _Ungoogled Chromium_ debería tomar tu
historial y tus preferencias, así podés pasarte directo a usar un
navegador que sea un aliado de lxs piratas.

Si estabas usando Chrome, hay que copiar la carpeta de archivos de un
lado a otro,
[escribinos](https://utopia.partidopirata.com.ar/quiero_ser_pirata.html)
si necesitás una mano P)

Y ya que estamos... podemos instalar [µBlock
Origin](https://getublock.com/) para bloquear publicidades y navegar con
más tranquilidad.
