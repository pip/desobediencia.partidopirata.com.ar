---
title: El autoritarismo en tiempos de la pandemia
description: |
  Sobre el posible paso del Estado de Emergencia hacia una Economía del
  Control Social
author:
- Colectivo Disonancia
categories:
- Chile
layout: post
---

![doctor_peste](https://colectivodisonancia.net/wp-content/uploads/2020/04/Portada_Autoritarismo.jpg)

Nos encontramos en un momento en el que la vida cotidiana se ha visto
afectada en diferentes aspectos, causando en la mayoría un ánimo de
incertidumbre. Las alertas del mercado global anuncian la proximidad de
una crisis económica que ya era esperada, mientras que la precariedad de
los sistemas públicos de salud muestran la insoportable desigualdad
económica con que en algunos países enfrentamos la enfermedad. Cada vez
es más evidente que el peligro de esta pandemia depende de en qué clase
social estamos.

Además de la amenaza a la salud que significa el virus, cada día
aparecen también nuevas señales de otra amenaza, en este caso
directamente política, ligada a las formas autoritarias de enfrentar la
pandemia. En Hungría, la democracia liberal ha acabado y un [nuevo
régimen con enormes atribuciones
presidenciales](https://www.bbc.com/mundo/noticias-internacional-52151321),
sin parlamento ni elecciones, fue entregado a Víktor Orbán por tiempo
indefinido. En Israel se han cerrado los tribunales y [el gobierno
autorizó a su agencia de
seguridad](https://www.nytimes.com/es/2020/04/01/espanol/coronavirus-decreto-emergencia-autocratas.html)
para que vigile a todos los ciudadanos por medio de los datos recabados
de sus dispositivos móviles. En China, donde empezó el contagio, [se dan
los casos más extremos de
vigilancia](https://www.eldiario.es/theguardian/vigilancia-ciudadanos-China-colateral-coronavirus_0_1004050368.html):
la ubicación de cada ciudadano es rastreada por medio de los datos de su
teléfono y por el reconocimiento facial en las calles, además de las
medidas policiales para fiscalizar y "ejercer" las normas de circulación
pública.

Dadas las condiciones actuales de la pandemia y de la incertidumbre
médica y política sobre cuánto se extenderán sus efectos, todo pareciera
apuntar a que los Estados de Emergencia permitirán con mayor facilidad
la proliferación de medidas autoritarias como las señaladas
anteriormente. Autores como [Byung-Chul
Han](https://elpais.com/ideas/2020-03-21/la-emergencia-viral-y-el-mundo-de-manana-byung-chul-han-el-filosofo-surcoreano-que-piensa-desde-berlin.html)
y [Yuval Noah
Harari](https://telegra.ph/Yuval-Noah-Harari-El-mundo-después-del-coronavirus-04-13),
anuncian ya que existe una gran posibilidad de que estas medidas
autoritarias perduren en el tiempo, se extiendan por el planeta
y reconfiguren el escenario político global. Si bien hay suficiente
indicios para pensar lo mismo, la incertidumbre que queda entonces es
sobre cuál va a ser el grado de la transformación política y cuán
arraigada será esta transformación en la sociedad y en la
historia. Podría tratarse de un aumento notorio del autoritarismo o una
transformación estructural del capital. Del mismo modo, por muy
radicales que fuesen las medidas, tal vez no lleguen a estar lo
suficientemente arraigadas como para perdurar. Cualquiera sean las
opciones, hay que considerar que, dada la rapidez de las decisiones
y eventos en los que nos encontramos, toda conjetura podría quedar en
una mera posibilidad nunca realizada, sin embargo es preferible estar
alertas, sobre todo si la amenaza en camino pudiera ser mayor que un
virus.

Ante esto, vemos la posibilidad de que la pandemia sea el detonante para
el surgimiento visible de una nueva forma de dominación tecnocrática que
sea incluso capaz de transformar la dominación y producción capitalista
tal como la conocemos hasta ahora. Particularmente, entendemos
tecnocracia como el poder ejercido a través de la administración de la
sociedad por medio del control técnico, ya sea como argumento de
"experticia" o como fuerza política.



## De la crisis a la economía del control

El capitalismo entra en crisis estructuralmente, esa es una de sus
características; del mismo modo, posee una enorme capacidad para superar
las crisis y salir fortalecido. El COVID-19 podría ser una anécdota más
en su historia, pero ciertamente le ha afectado por la situación en la
que se encuentra. Como [señala David
Harvey](https://ctxt.es/es/20200302/Politica/31496/coronavirus-anticapitalismo-neoliberalismo-medidas-covid19-david-harvey-jacobin.htm),
la dificultad ya existente en el mercado para encontrar la demanda
suficiente para "justificar" el capital ficticio y la deuda, la
paralización de los mercados orientados al consumo inmediato y la
devaluación creciente de toda la mercancía debido a la incertidumbre
sobre cuánto durarán las restricciones de emergencia, muestran un
escenario extremadamente desfavorable para el capitalismo.

De permanecer una situación como esta, se requerirá mucho más que la
"autorregulación del mercado" para salir de la crisis, lo que puede
favorecer la adopción progresiva de las medidas de emergencia y de
control extremo. China es un ejemplo de una economía que, a pesar de
contar con todas las características del mercado capitalista, está
hegemonizada por medidas de control y planificación central (o
corporativa) que dejan fuera incluso la abstracta expresión de la
"libertad de los agentes del mercado" para introducir el control directo
sobre la vida de las personas como un factor esencial. El [puntaje
social en
China](https://www.eldiario.es/sociedad/China-pone-buenos-malos-ciudadanos_0_831866905.html)
no solo es una medida de administración policial, es también un factor
de disciplinamiento de la fuerza laboral y una directriz sobre los
hábitos de producción y consumo en el país. En una situación de
emergencia prolongada, medidas de control de este tipo podrían ser
requeridas si las empresas empiezan a forzar la actividad económica y,
a la vez, un mayor disciplinamiento laboral. De esta manera, aquellas
medidas autoritarias que vemos como reacción a la pandemia, podrían
enraizar en la sociedad y perdurar por tiempo indefinido si logran
traspasar la emergencia inmediata y se mantienen como medias de
emergencia económica, particularmente si los mecanismos de vigilancia en
China empiezan a tener mayor popularidad en la prensa y en la opinión
política occidental.

Aun así, de extenderse las medidas de control del ámbito médico al
económico, la inmediatez de la situación podría hacer que el aumento de
las medidas autoritarias fueran pasajeras. Sin embargo, la pandemia
podría estar acelerando el proceso de deriva totalitaria que ya se podía
ver en el Estado y en el propio mercado. La oleada de incorporación de
leyes antiterroristas y medidas de excepción dentro de los Estados de
Derecho en tiempos "normales" que hemos visto en las últimas
décadas[^cd1], da cuenta de un prolongado proceso político reaccionario
que revela la incomodidad del capitalismo actual frente a las libertades
y derechos que los propios liberales inventaron y usaban como argumento
de los supuestos valores del capitalismo; valores que podrían estar
dispuestos a desechar.

En términos económicos, el corporativismo, como el fenómeno de grandes
concentraciones de capital en empresas multinacionales, ha creado
enormes instituciones, principalmente las corporaciones ligadas al
desarrollo tecnológico, con una capacidad equivalente a Estados
multinacionales. Google, Facebook, Apple, entre otras, tienen la
capacidad productiva y la influencia directa en las personas suficientes
como para ser actores políticos decisivos en el actual escenario
global. Si los algoritmos de estas corporaciones llegan a tener la
atribución de decidir sobre qué libertades o derechos debe restringirse
a una persona, decisión que pueden justificar con el argumento de que
tienen los datos para saber la "realidad" de cada persona, estaremos en
la antesala de una forma de dominación tecnocrática. El modelo de
control aplicado en China no depende, en su esencia, del rol del Estado,
sino de que los factores del poder tecnocrático tengan la capacidad
y legitimidad suficiente para poder incidir en la sociedad, la política
y la economía; un mercado autoritario o un capitalismo tecnocrático es
posible[^cd2].


## La lucha que nos espera


El poder tecnocrático consistiría en la movilización coordinada de las
instituciones de la sociedad –-mercado, estado, familia, etc-– para
mantener el orden y el control como principio esencial de los aspectos
de la vida: política, cultura, biología. Aquello que podíamos intuir
como control social en la vigilancia masiva y en el poder de influencia
de los monopolios, pasaría a formar parte de la política pública. Ante
la amenaza de una emergencia permanente, o el temor de que la
inestabilidad pueda convertirse en una emergencia; el orden, la
vigilancia y el control pasarían a ser los lemas de una nueva forma de
dominación y, también, los nuevos valores públicos.

Debemos tener cuidado con los discursos que, en nombre del beneficio de
toda la humanidad, hagan de estos elementos de control el motivo central
de la vida social, imponiéndose con la pretensión de que no hay más
alternativas que discutir. Del mismo modo, las grandes soluciones
tecnológicas, por muy "abiertas" y colaborativas que se declaren, si son
administradas por corporaciones, siempre son un peligro para la
autonomía de las comunidades ya que le entregan un poder inmenso las
instituciones sobre las cuales no tenemos ninguna capacidad de decisión.

Esta nueva forma de dominación no solo es una amenaza por su capacidad,
sino que también lo es porque bajo su argumento de la correcta
administración de los recursos sociales, económicos y tecnológicos,
podría ser percibida como una política progresista más. Sin embargo, el
contenido autoritario de este poder no está necesariamente declarado en
su discurso, sino que se encuentra en su capacidad de obtener control
directo sobre todos los aspectos de la sociedad. Quienes estamos en la
lucha anticapitalista radical, debemos ser capaces de ver a la
tecnocracia en los mismos términos de una clase social dominante a la
que enfrentar[^cd3]. Es momento de también ser antitecnocráticos.

La pandemia nos ha causado un daño evidente, directamente a la salud
o por medio de la amenaza cada vez más real de un período extenso de
precarización de la vida. Debemos enfrentarla y fortalecer los lazos
comunitarios y políticos, sin dejar de perder de vista la capacidad del
capitalismo y de los tecnócratas para tomar la iniciativa y aprovechar
la emergencia para impulsar una nueva forma de dominación.

En este sentido, es urgente dar la reflexión y discusión crítica en
torno al origen de esta pandemia que, como todas en la historia, guarda
una profunda relación con la industria alimenticia animal[^cd4], debemos
poner en duda el actual modo de producción de alimentos, su lógica
capitalista y su amenaza permanente para nuestro futuro. Por otro lado,
hoy más que nunca, debemos esclarecer nuestros horizontes políticos
a largo plazo pensando en qué modelo de sociedad queremos vivir y qué
estrategias políticas necesitamos para conseguirlo. Necesitamos esta
reflexión no solo para poder presentar alternativas viables y globales
a la producción saludable de alimentos, sino también porque el poder
burocrático puede validarse como representante de los intereses de toda
la humanidad por medio de la seguridad, la salud y el orden. Saber
oponernos a las medidas de control social y diferenciarnos de sus
propuestas dependerá de la claridad de nuestro horizonte
estratégico. Finalmente, debemos seguir construyendo una organización
con objetivos radicales que sea capaz de enfrentar los desafíos. La
experiencia de la revuelta chilena de los últimos meses muestra que
incluso en un panorama político opresivo y enajenante como el de Chile
previo a las protestas, es posible un levantamiento masivo contra todo
pronóstico, por lo que siempre es posible una sublevación.

En la historia hemos experimentado muchas pandemias y superaremos esta;
a la dominación que está por venir, si estamos organizados y tenemos
claros nuestros objetivos, también.


[^cd1]: En Chile el 2019, por ejemplo, [fue aprobado una "vía rápida"
  dentro del Código Procesal
  Penal](https://www.senado.cl/senado-aprueba-en-general-ley-corta-antiterrorista/senado/2019-08-07/170817.html)
  que permite a las policías y al poder jurídico atribuir rápidamente
  como terrorismo investigaciones penales sin tener que pasar por el
  procedimiento regular para definir un acción terrorista. En varios
  países del mundo pueden encontrarse casos similares, originalmente
  motivadas por el ejemplo del "Ley Patriota" en Estados Unidos desde
  el 2001.

[^cd2]: El economista John Kennet Galbraith ya a fines de los 60'
  describió los procesos de monopolio y burocratización de las grandes
  corporaciones y cómo, además de los efectos económicos del monopolio,
  conllevaban el surgimiento de un poder político corporativo.

[^cd3]: Carlos Pérez Soto ha formulado la idea de una nueva clase social
  dominante, la Burocracia, que por medio de la gestión del conocimiento
  o "saber" sobre las nuevas tecnologías de la producción, es capaz de
  disputar la hegemonía social y económica a los capitalistas.

[^cd4]: Un análisis detallado y documentado sobre el desarrollo
  histórico de las epidemias y su relación con la producción de
  alimentos, y particular la ganadería, se puede encontrar en el
  capítulo "El regalo mortal del ganado" en _Armas, gérmenes y acero_
  (1997), de Jared Diamond.

