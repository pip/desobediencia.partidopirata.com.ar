all: build deploy

build:
	JEKYLL_ENV=production bundle exec jekyll build

deploy:
	rsync -rvzuP --chmod=750 -og --chown=root:http _site/ root@partidopirata.com.ar:/srv/http/desobediencia.partidopirata.com.ar
